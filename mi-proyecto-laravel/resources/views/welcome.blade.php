<!doctype html>
<html lang="es">
<head>
    <style type="text/css">
        @keyframes animacion{
            0%{
                left: 0px;
            }
            25%{
                left: 1250px;
                top: 0px;
                background-color: yellow;
            }
            50%{
                left: 1250px;
                top: 530px;
                background-color: springgreen;
            }
            75%{
                top: 530px;
                left:0px;
                background-color: mediumvioletred;
            }
            100%{
                left: 100px;
                top: 0px;
                background-color: aqua;
            }
        }
        div {
            width: 100px;
            height: 100px;
            background: orangered;
            position: relative;
            animation-duration: 10s;
            animation-name: animacion;
            animation-iteration-count: infinite;
            animation-direction: alternate;
        }
        </style>
</head>
<body>
<div>

</div>

</body>
</html>

