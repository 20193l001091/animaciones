<!doctype html>
<html lang="es">
<head>
    <style>
        div {
            width: 100px;
            height: 100px;
            background-color: indianred;
            top: -100px;
            transition: width 4s, height 4s, transform 4s;
            left: 50px;
        }

        div:hover {
            width: 300px;
            height: 300px;
            transform: rotate(180deg);
        }
    </style>
</head>
<body>
<div>

</div>

</body>
</html>

